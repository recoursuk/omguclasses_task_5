package MatrixServices;

import Matrix.IMatrix;

import java.util.Comparator;

public class MatrixComparator implements Comparator<IMatrix> {
    @Override
    public int compare(IMatrix o1, IMatrix o2) {
        return Double.compare(o1.getDet(),o2.getDet());
    }
}
