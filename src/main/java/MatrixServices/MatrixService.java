package MatrixServices;

import Matrix.Matrix;

import java.util.Arrays;
import java.util.Comparator;

public class MatrixService {
    public static void arrangeMatrices(Matrix... arr){
        Arrays.parallelSort(arr, new MatrixComparator());
    }
}
