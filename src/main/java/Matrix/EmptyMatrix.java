package Matrix;

import java.util.Arrays;
import java.util.Objects;

abstract public class EmptyMatrix implements IMatrix {
    protected double[] arr;
    protected double detCache;
    protected boolean detCalculatedFlag = false;

    public EmptyMatrix(int n){
        this.arr =new double[n];
        Arrays.fill(arr,0);
    }

    public IMatrix fillRand(){
        for(int i=0; i<arr.length; i++){
            arr[i] = Math.random()*10;
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmptyMatrix that = (EmptyMatrix) o;
        return Double.compare(that.detCache, detCache) == 0 &&
                detCalculatedFlag == that.detCalculatedFlag &&
                Arrays.equals(arr, that.arr);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(detCache, detCalculatedFlag);
        result = 31 * result + Arrays.hashCode(arr);
        return result;
    }
}
