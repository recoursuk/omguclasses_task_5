package Matrix;

public class DigMatrix extends EmptyMatrix implements IMatrix{

    public DigMatrix(int n){
        super(n);
    }

    @Override
    public double getElem(int x, int y){
        if(x!=y) {
            throw new IllegalArgumentException("It's diagonal matrix");
        }
        return arr[x];
    }

    @Override
    public void changeElem(int x, int y, double changer){
        if( x!=y ) {
            throw new IllegalArgumentException("It's diagonal matrix");
        }
        arr[x] = changer;
        detCalculatedFlag = false;
    }

    @Override
    public double getDet() {
        if (!detCalculatedFlag){
            detCache = 1;
            for(double e:arr){
                detCache *= e;
            }
            detCalculatedFlag = true;
        }
        return detCache;
    }

    @Override
    public int getLength() {
        return arr.length;
    }
}
