package Matrix;

public interface IMatrix {
    double getElem(int x, int y) ;
    void changeElem(int x, int y, double changer);
    double getDet();
    int getLength();
}
