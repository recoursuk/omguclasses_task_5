package Matrix;

public class Matrix extends EmptyMatrix implements IMatrix {

    public Matrix(int n){
        super(n*n);
    }

    public double getElem(int x, int y) {
        return arr[(int) (x * getLength())+y];
    }

    public void changeElem(int x, int y, double changer) {
        this.arr[(int) (x*getLength()) +y] = changer;
        detCalculatedFlag = false;
    }

    private static void swapRows(int i1, int i2, Matrix m){
        for(int i = 0; i<m.getLength(); i++){
            double tmp = m.arr[i1*m.getLength()+i];
            m.arr[i1 * m.getLength() + i] = m.arr[i2 * m.getLength() +i];
            m.arr[i2 * m.getLength() + i] = tmp;
        }
    }

    public Matrix copy(){
        Matrix newm = new Matrix(getLength());
        newm.arr = arr.clone();
        return newm;
    }

    private static double transFromToCalc(Matrix m, int recIndex){
        double prefix = 1;
        if(recIndex + 1 == m.getLength()){
            return m.getElem(recIndex,recIndex);
        }
        for(int i = recIndex ;i + 1 < m.getLength() && m.getElem(recIndex,recIndex) == 0 ;++i){
            if(m.getElem(i + 1,recIndex)==0){
                continue;
            }
            swapRows(recIndex,i + 1,m);
            prefix*=-prefix;
        }
        //если поменять не удалось т.е. содержится столбец из 0 а значит определитель 0
        if(m.getElem(recIndex,recIndex) == 0){
            return 0;
        }
        //приводим первые элементы строк к нулю значит строки где первый элемент 0 не трогаем
        //сначала все первые элементы приводим к 1
        for(int i = recIndex;i < m.getLength();++i){
            //строки где первый элемент уже 0 не трогаем
            if(m.getElem(i,recIndex) != 0) {
                double div = m.getElem(i,recIndex);
                prefix *= div;
                for (int j = recIndex; j < m.getLength(); ++j) {
                    m.changeElem(i, j, (m.getElem(i, j) / div));
                }
            }
        }
        for(int i = recIndex + 1;i < m.getLength();++i){
            if(m.getElem(i,recIndex) != 0) {
                for (int j = recIndex; j < m.getLength(); ++j) {
                    m.changeElem(i, j, m.getElem(i, j) - m.getElem(recIndex, j));
                }
            }
        }
        return prefix;
    }

    public double getDet(){
        if(!detCalculatedFlag) {
            Matrix tmp = this.copy();
            detCache = 1;
            for (int recIndex = 0; recIndex < getLength() && detCache != 0; recIndex++){
                detCache *= transFromToCalc(tmp, recIndex);
            }
            detCalculatedFlag = true;
        }
        return detCache;
    }

    @Override
    public int getLength() {
        return (int) Math.sqrt(arr.length);
    }
}
