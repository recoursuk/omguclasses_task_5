package Matrix;

public class UpTriangleMatrix extends EmptyMatrix implements IMatrix{

    private int length;

    public UpTriangleMatrix(int n) {
        super((n*n+n)/2);
        length = n;
    }

    @Override
    public double getElem(int x, int y) {
        if(x < y) {
            throw new IllegalArgumentException("It's diagonal matrix");
        }
        return arr[x*(getLength()-x)+y];
    }

    @Override
    public void changeElem(int x, int y, double changer) {
        if(x < y) {
            throw new IllegalArgumentException("It's diagonal matrix");
        }
        arr[x * (getLength()-x) +y] = changer;
        detCalculatedFlag = false;
    }

    @Override
    public double getDet() {
        if(!detCalculatedFlag){
            for(int i = 0; i < getLength(); i++){
                detCache *= getElem(i,i);
            }
            detCalculatedFlag = true;
        }
        return detCache;
    }

    @Override
    public int getLength() {
        return length;
    }
}
