import Matrix.Matrix;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import Matrix.EmptyMatrix;

public class TestMatrix {

        @Test
        public void getDet1() throws Exception {
            EmptyMatrix m = new Matrix(1);
            m.changeElem(0,0,1);
            Assertions.assertEquals(m.getElem(0,0),1,1e-10);
            Assertions.assertEquals(m.getDet(),1,1e-10);
        }

        @Test
        public void getDet2() throws Exception {
            EmptyMatrix m1 = new Matrix(2);
            m1.changeElem(0,0,1);
            m1.changeElem(0,1,2);
            m1.changeElem(1,0,3);
            m1.changeElem(1,1,4);

            Assertions.assertEquals(m1.getDet(),-2,1e-10);
        }

        @Test
        public void getDet3() throws Exception {
            EmptyMatrix m = new Matrix(2);
            m.changeElem(0,0,0);
            m.changeElem(0,1,2);
            m.changeElem(1,0,3);
            m.changeElem(1,1,4);

            Assertions.assertEquals(m.getDet(),-6,1e-10);
        }

        @Test
        public void getDet4() throws Exception {
            EmptyMatrix m = new Matrix(2);
            m.changeElem(0,0,0);
            m.changeElem(0,1,0);
            m.changeElem(1,0,0);
            m.changeElem(1,1,0);

            Assertions.assertEquals(m.getDet(),0,1e-10);
        }

        @Test
        public void getDet5() throws Exception {
            EmptyMatrix m = new Matrix(2);
            m.changeElem(0,0,0);
            m.changeElem(0,1,0);
            m.changeElem(1,0,1);
            m.changeElem(1,1,1);

            Assertions.assertEquals(m.getDet(),0,1e-10);
        }

        @Test
        public void getDet6() throws Exception {
            EmptyMatrix m = new Matrix(2);
            m.changeElem(0,0,0);
            m.changeElem(0,1,1);
            m.changeElem(1,0,0);
            m.changeElem(1,1,1);

            Assertions.assertEquals(m.getDet(),0,1e-10);
        }


        @Test
        public void getDet7() throws Exception {
            EmptyMatrix m = new Matrix(3);
            m.changeElem(0,0,1);
            m.changeElem(0,1,2);
            m.changeElem(0,2,3);
            m.changeElem(1,0,4);
            m.changeElem(1,1,5);
            m.changeElem(1,2,6);
            m.changeElem(2,0,45);
            m.changeElem(2,1,-5);
            m.changeElem(2,2,11);

            Assertions.assertEquals(m.getDet(),-198,1e-10);
        }


    }
