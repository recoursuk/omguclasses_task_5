import Matrix.IMatrix;
import Matrix.Matrix;
import MatrixServices.MatrixComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestMatrixComparator {
    IMatrix m1 = new Matrix(5).fillRand();
    IMatrix m2 = new Matrix(5).fillRand();
    IMatrix m3 = new Matrix(1);
    IMatrix m4 = new Matrix(1);



    @Test
    void comparatorDemo(){
        int res = new MatrixComparator().compare(m1,m2);
        Assertions.assertTrue(
                (m1.getDet() < m2.getDet() && res < 0) ^
                ( m1.getDet() > m2.getDet() && res > 0 )^
                (Double.compare(m1.getDet(), m2.getDet()) == 0 && res ==0)
        );
    }

    @Test
    void comparatorTest(){
        m3.changeElem(0,0,5);
        m4.changeElem(0,0,5);
        Assertions.assertEquals(0, new MatrixComparator().compare(m3, m4));
        m3.changeElem(0,0,1);
        Assertions.assertEquals(-1,new MatrixComparator().compare(m3,m4));
        Assertions.assertEquals(1,new MatrixComparator().compare(m4,m3));

    }


}
